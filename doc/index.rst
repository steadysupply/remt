
Table of Contents
-----------------

.. toctree::
   :maxdepth: 3

   intro
   indexer

* :ref:`genindex`
* :ref:`search`

.. vim: sw=4:et:ai
