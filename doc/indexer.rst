PDF Annotations Indexer
=======================

Command example::

    $ remt index %i | rst2html - > doc-index.html

.. vim: sw=4:et:ai
